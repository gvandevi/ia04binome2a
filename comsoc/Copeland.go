package comsoc

func CopelandSWF(p Profile) (count Count, err error) {
	count = make(Count)
	alts := make([]Alternative, 0)
	for i := 1; i <= len(p[0]); i++ {
		alts = append(alts, Alternative(i))
	}
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	for alt1 := 1; alt1 <= len(alts); alt1++ {
		for alt2 := alt1 + 1; alt2 <= len(alts); alt2++ {
			score1, score2 := 0, 0
			for _, pref := range p {
				if isPref(Alternative(alt1), Alternative(alt2), pref) {
					score1++
				} else {
					score2++
				}
			}
			if score1 > score2 {
				count[Alternative(alt1)]++
				count[Alternative(alt2)]--
			} else if score2 > score1 {
				count[Alternative(alt1)]--
				count[Alternative(alt2)]++
			}
		}
	}
	return
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), err
}
