package comsoc

import (
	"math/rand"
	"time"
)

func GenerateProfile(nVoters int, nAlts int)(profil Profile) {
	// Initialisation du profil
	profil = make([][]Alternative, nVoters)
	for i := range profil {
		profil[i] = make([]Alternative, nAlts)
	}

	// Définition des alternatives
	values := make([]Alternative, nAlts)
	for i := range values {
		values[i] = Alternative(i + 1)
	}

	// Remplissage du profil avec des permutations différentes à chaque rang
	rand.Seed(time.Now().UnixNano())
	for i := range profil {
		// Mélange les valeurs pour chaque ligne
		permutation := make([]Alternative, len(values))
		copy(permutation, values)
		rand.Shuffle(len(permutation), func(i, j int) {
			permutation[i], permutation[j] = permutation[j], permutation[i]
		})

		// Remplit la ligne avec la permutation
		copy(profil[i], permutation)
	}
	return profil
}