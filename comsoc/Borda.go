package comsoc

import (
	"fmt"
)

func BordaSWF(p Profile) (count Count, err error) {
	count = make(Count)
	err = checkProfileFromProfile(p)
	if err != nil {
		return nil, err
	}
	nAlts := len(p[0])
	for _, row := range p {
		for i := 0; i < nAlts; i++ {
			count[row[i]] += nAlts - 1 - i
		}
	}
	return
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), err
}

func Test_borda() {
	profil := GenerateProfile(3, 5)
	fmt.Println("Profil :", profil)
	count, _ := BordaSWF(profil)
	fmt.Println("Décompte :", count)
	winners, _ := BordaSCF(profil)
	fmt.Println("Vainqueur(s) :", winners)
}
