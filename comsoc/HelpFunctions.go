package comsoc

import (
	"errors"
	"fmt"
)

func plusN(n int) func(i int) int {
	f := func(i int) int {
		return (i + n)
	}
	return f
}

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}
func Test_rank() {
	tableau := []Alternative{1, 2, 3, 4, 5}
	indice := rank(tableau[3], tableau)
	fmt.Print(indice)
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	// if (alt1 <0 || alt2 <0){
	// 	return errors.New("l'une des deux valeurs n'est pas présente dans le tableau")
	// }else {
	return rank(alt1, prefs) < rank(alt2, prefs)
}

// }
func Test_isPref() {
	tableau := []Alternative{1, 2, 3, 4, 5}
	alt1 := Alternative(1)
	alt2 := Alternative(2)
	response := isPref(alt1, alt2, tableau)
	fmt.Print(response)
}

// renvoie les meilleures alternatives pour un décompte donné
func maxCount(count Count) (bestAlts []Alternative) {
	maxPoints := -1
	for i, points := range count {
		if points > maxPoints {
			bestAlts = []Alternative{Alternative(i)}
			maxPoints = points
		} else if points == maxPoints {
			bestAlts = append(bestAlts, Alternative(i))
		}
	}
	return bestAlts
}
func Test_maxCount() {
	count := make(map[Alternative]int)
	count[Alternative(1)] = 3
	count[Alternative(2)] = 5
	count[Alternative(3)] = 5
	fmt.Println(maxCount(count))
}

// // vérifie les préférences d'un agent, par ex.
// qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func checkProfile(prefs []Alternative, alts []Alternative) error {
	//vérifier
	if len(prefs) < len(alts) {
		return errors.New("préférences non complètes")
	} else if len(prefs) > len(alts) {
		return errors.New("alternatives en trop")
	} else { //vérifier complet
		for _, element := range alts {
			if rank(element, prefs) == -1 {
				return errors.New("une des alternatives est absente des préférences")
			}
			nApp := 0
			for _, pref := range prefs {
				if element == pref {
					nApp += 1
				}
			}
			if nApp != 1 {
				return errors.New("une alternative apparait plusieurs fois")
			}
		}
	}
	return nil
}

func Test_checkProfile() {
	alts := []Alternative{1, 2, 3, 4, 5}
	//prefs := []Alternative{3, 1, 5, 2, 4}
	//prefs := []Alternative{1, 2, 3, 4, 5,6}
	prefs := []Alternative{1, 2, 3, 4, 1}
	fmt.Println(checkProfile(prefs, alts))
}

// vérifie le profil donné, par ex. qu'ils sont tous complets
// et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for _, pref := range prefs {
		return checkProfile(pref, alts)
	}
	return nil
}

func checkProfileFromProfile(prof Profile) (err error) {
	alts := make([]Alternative, 0)
	for i := 1; i <= len(prof[0]); i++ {
		alts = append(alts, Alternative(i))
	}
	err = checkProfileAlternative(prof, alts)
	return
}

func Test_checkProfileAlternative() {
	alts := []Alternative{1, 2, 3, 4, 5}
	pref1 := []Alternative{5, 3, 1, 4, 2}
	pref2 := []Alternative{1, 5, 4, 3, 2}
	Pref := [][]Alternative{pref1, pref2}
	profil := Profile(Pref)
	fmt.Println(checkProfileAlternative(profil, alts))
}

func Test_MajoritySWF() {
	pref1 := []Alternative{1, 2, 3}
	pref2 := []Alternative{2, 3, 1}
	pref3 := []Alternative{3, 1, 2}
	Pref := [][]Alternative{pref1, pref2, pref3}
	profil := Profile(Pref)
	fmt.Println(MajoritySWF(profil))
	c, _ := ApprovalSWF(profil, []int{1, 2, 3, 4, 5})
	fmt.Println(c[Alternative(2)])
	fmt.Println(CondorcetWinner(profil))
}
