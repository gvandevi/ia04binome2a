package comsoc

func getRandomKey(count Count) Alternative {
	for k := range count {
		return k
	}
	return 0 // should never happen
}

func countContains(count Count, alt Alternative) bool {
	for k := range count {
		if alt == k {
			return true
		}
	}
	return false
}

// renvoie une des pires alternatives pour un décompte donné
func minCount(count Count, alts []Alternative) (worstAlt Alternative) {
	for _, alt := range alts {
		if !countContains(count, alt) {
			return alt
		}
	}
	worstAlt = getRandomKey(count)
	minPoints := count[worstAlt]
	for i, points := range count {
		if points < minPoints {
			worstAlt = i
			minPoints = points
		}
	}
	return worstAlt
}

func STV_SWF(p Profile) (count Count, err error) {
	count = make(Count)
	alts := make([]Alternative, 0)
	for i := 1; i <= len(p[0]); i++ {
		alts = append(alts, Alternative(i))
	}
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}

	nbRounds := len(alts)
	for round := 0; round < nbRounds; round++ {
		majorityRes := make(Count)
		for _, pref := range p {
			majorityRes[pref[0]]++
		}
		worstAlt := minCount(majorityRes, alts)
		count[worstAlt] = round

		// On enlève la pire alt des alts
		alts[rank(worstAlt, alts)] = alts[len(alts)-1]
		alts = alts[:len(alts)-1]

		// on enlève la pire alt de chacunes des preferences
		for i, pref := range p {
			pref[rank(worstAlt, pref)] = pref[len(pref)-1]
			pref = pref[:len(pref)-1]
			p[i] = pref
		}
	}
	return
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := STV_SWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), err
}
