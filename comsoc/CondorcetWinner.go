package comsoc

import "errors"

// Gagnant de condorcet, retourne un slice vide ou de 1 élément
// A vérifier avec plus d'exemples
func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	for _, alt1 := range p[0] {
		winner := true
		for _, alt2 := range p[0] {
			if alt1 != alt2 {
				nbAlt1 := 0
				nbAlt2 := 0
				for _, pref := range p {
					if isPref(alt1, alt2, pref) {
						nbAlt1++
					} else {
						nbAlt2++
					}
				}
				if nbAlt1 <= nbAlt2 {
					winner = false
				}
			}
		}
		if winner {
			return []Alternative{alt1}, nil
		}
	}
	return nil, errors.New("pas de gagnant de Condorcet")
}
