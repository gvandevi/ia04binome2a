package ia04binome2a

type BallotRequest struct {
	Rule     string   `json:"rule"`
	Deadline string   `json:"deadline"`
	VotersID []string `json:"voters-id"`
	NbAlts   int      `json:"#alts"`
	TieBreak []int    `json:"tie-break"`
}

type Vote struct {
	AgentID  string `json:"agent-id"`
	BallotID string `json:"ballot-id"`
	Prefs    []int  `json:"prefs"`
	Options  []int  `json:"options"`
}

type Ballot struct {
	BallotID string `json:"ballot-id"`
}

type ResultResponse struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}

type Request interface {
	BallotRequest | Ballot | Vote
}
