package main

import (
	"fmt"

	va "gitlab.utc.fr/gvandevi/ia04binome2a/agt/voteragent"
)

func main() {
	ag := va.NewRestClientAgent("ag1", "http://localhost:8080", "scrutin1", 3, make([]int, 0), make([]int, 0))
	ag.Start()
	fmt.Scanln()
}
