package main

import (
	"gitlab.utc.fr/gvandevi/ia04binome2a/comsoc"
)

func main() {
	//Fonctions utilitaires
	//comsoc.Test_rank()
	//comsoc.Test_maxCount()
	//comsoc.Test_checkProfile()
	//comsoc.Test_checkProfileAlternative()
	//comsoc.Test_MajoritySWF()
	//comsoc.Test_sWFFactory()
	comsoc.Test_majority()
}
