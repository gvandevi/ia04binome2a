package main

import (
	"fmt"

	ba "gitlab.utc.fr/gvandevi/ia04binome2a/agt/ballotagent"
)

func main() {
	server := ba.NewBallotServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
